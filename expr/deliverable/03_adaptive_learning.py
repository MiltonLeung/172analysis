import sys
import motion_util as u
import numpy as np
import matplotlib.pyplot as plt

def to_label(info,movement):
    for i in range(len(movement)):
        if  info[0] >= movement[i][0] and info[1] <= movement[i][1] :
            return movement[i][2]
    return 'other'

feature=sys.argv[1]
label=sys.argv[2]
wristband=[]
movement=[]
normalized_vector=[]
print feature,label
for line in open(feature,'r'):
    normalized_vector.append({'info':map(lambda x:float(x),line.split(',')[0:3]),'features':map(lambda x:float(x),line.split(',')[5:])})
for line in open(label,'r'):
    record=line.split(',')
    start=float(record[0])
    end=float(record[1])
    target=record[2]
    movement.append((start,end,target))

# normalized_vector=reduce(lambda x,y:x+y,[a.normalize_vector(a.get_movement_feature(key,a.get_movement_segment(key,0.1))) for key in a.sample.keys()],[])

# movement=[(1485409440,1485409618,'standing'),(1485409620,1485409978,'walking'),(1485409980,1485410098,'sitting'),(1485410100,1485410458,'jumping')]
import pandas
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
# load dataset
X = map(lambda x:x['features'],normalized_vector) 
Y = filter(lambda x:x!='other',map(lambda x:to_label(x['info'],movement),normalized_vector))

# prepare configuration for cross validation test harness
seed = 7
# prepare models
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
# evaluate each model in turn
results = []
names = []
scoring = 'accuracy'
for name, model in models:
	kfold = model_selection.KFold(n_splits=6, random_state=seed)
	cv_results = model_selection.cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	print(msg,list(cv_results))
# boxplot algorithm comparison
fig = plt.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show()