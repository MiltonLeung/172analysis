import sys
import motion_util as u
import numpy as np
import matplotlib.pyplot as plt
infile=sys.argv[1]
wristband=[]
if (len(sys.argv)>2):
    wristband+=sys.argv[2].split(',')
print infile,wristband
a=u.motionUtil(wristband,windows=2.5)
a.fetch_data_from_csv(infile)
cluster=6
normalized_vector=reduce(lambda x,y:x+y,[a.normalize_vector(a.get_movement_feature(key,a.get_movement_segment(key,0.08))) for key in a.sample.keys()],[])
idx=a.PCA_kmeans(normalized_vector,cluster)
for i in range(cluster):
    cat_feature=filter(lambda x:x[1]==i,zip(normalized_vector,idx))
    print "Category %s characteristics" % str(a.color[i])
    fig = plt.figure( figsize=(17, 2*(len(cat_feature)/3+1)), dpi=80)
    for i in range(len(cat_feature)):
        obs = cat_feature[i][0]
        label = cat_feature[i][1]
        start=obs['info'][0]
        end=obs['info'][1]
        avc=obs['info'][2]
        w=obs['info'][4]
        m=a.filter_motion(a.sample[w],start,end)
        plt.subplot(len(cat_feature)/3+1,3,i+1)
        # plt.ylim(-4, 4)
        a.motion_plot(m,plt)
plt.tight_layout()
plt.show()