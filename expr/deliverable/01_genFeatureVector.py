import sys
import motion_util as u
import numpy as np
import matplotlib.pyplot as plt
infile=sys.argv[1]
outfile=sys.argv[2]
wristband=[]
if (len(sys.argv)>3):
    wristband+=sys.argv[3].split(',')
print infile,outfile,wristband
a=u.motionUtil(wristband,windows=2.5) 
a.fetch_data_from_csv(infile)
if(len(a.sample.keys())!=0):
    normalized_vector=reduce(lambda x,y:x+y,[a.normalize_vector(a.get_movement_feature(key,a.get_movement_segment(key,0.08))) for key in a.sample.keys()],[])
    with open(outfile,'w') as f:
        output='\n'.join(map(lambda y:','.join(map(lambda x:str(x),list(y['info'])+list(y['features']))),normalized_vector))
        f.write(output)