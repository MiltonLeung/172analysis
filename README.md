# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Piegon motion analytic suite
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Project is developed in Python2 whichs extracts motion data and plot motion trends.
* Configuration
pip install matplotlib
pip install mpld3
* Dependencies
info/wristband_info.txt
info/rpi_info.txt
* Database configuration
Nil
* How to run tests
python [-k KEY] csv2json.py <raw_datafile> <output directory>
python motion_plot.py <motion_json_file>
* Deployment instructions
Nil

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact