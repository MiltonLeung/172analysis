#  
# Plot session
#

# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import mpld3
import sys
from datetime import datetime
import json
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("infile", help="input the motion file")
parser.add_argument("outpath", help="specify output directory")
parser.add_argument('--html',dest='html',action='store_true',help="output as html file")
parser.set_defaults(html=False)
args = parser.parse_args()

infile=args.infile # sys.argv[1] # 'motion_20170119.json'
outfile_path=args.outpath # sys.argv[2]
json_str = open(infile,'r')
json_data=json.load(json_str)

# mpld3.enable_notebook()
# mpld3.disable_notebook()

ordered_keys=sorted(filter(lambda x:x!='file_name',json_data.keys()),key=lambda k:
                    (json_data[k]['room_number'],
                     json_data[k]['user_name'],
                     json_data[k]['hand'],
                    ))

def getDimension(length):
    nrow = int(length ** (0.5))
    ncol = int(length ** (0.5))
    if(ncol*nrow<length):
        nrow+=1
    if(ncol*nrow<length):
        ncol+=1
    # return [nrow,ncol]
    return [length-1,1]

fig = plt.figure(num=None, figsize=(50, 4*len(json_data)), dpi=80)
fig.subplots_adjust(hspace=0.1,wspace=0.1)
labelx = -0.3  # axes coords

suplot_idx=[getDimension(len(json_data))+[i] for i in range(1,len(json_data)+1)]
from_datetime=lambda x:datetime.fromtimestamp(float(x))

for key,idx in zip(ordered_keys,suplot_idx):
    m = key
    loc = json_data[key]['location_paths']
    loc_time=reduce(lambda x,y: x+y,
                    map(lambda x:[from_datetime(x['start_time']),from_datetime(x['end_time'])],loc),
                    [])
    uni_loc=set(map(lambda x: x['location'],loc))
    u = json_data[key]['user_name']
    h = json_data[key]['hand']
    r = json_data[key]['room_number']
    t = map(from_datetime,json_data[key]['t'])
    x = json_data[key]['x']
    y = json_data[key]['y']
    z = json_data[key]['z']
    # msg_idx = json_data[key]['idx']
    rt = map(from_datetime,json_data[key]['rt'])
    rx = json_data[key]['rx']
    ry = json_data[key]['ry']
    rz = json_data[key]['rz']
    start = str(t[0]).split('.')[0]
    end = str(t[-1]).split('.')[0]
    ax = fig.add_subplot(*idx)
    ax.plot(t,x, label='x')
    ax.plot(t,y, label='y')
    ax.plot(t,z, label='z')
    # ax.plot(t,msg_idx, label='idx')
    min_val=min(map(lambda x:float(x),x+y+z))
    max_val=max(map(lambda x:float(x),x+y+z))
    line_density=2
    time_dots=[min_val+i/float(line_density-1)*(max_val-min_val) for i in range(line_density)]
    time_lines=[ax.plot([xc]*line_density,time_dots,'r:',marker='.',markersize=15,color='blue' if i%2==0 else 'red')for xc,i in zip(loc_time,range(len(loc_time)))]
    labels=['location: '+l['location']+"<br>"+time+": "+str(from_datetime(l[time]).time()) for l in loc for time in ['start_time','end_time']]
    for t,l in zip(time_lines,labels):
        tooltip = mpld3.plugins.PointHTMLTooltip(t[0], [l]*line_density,voffset=10, hoffset=10)
        mpld3.plugins.connect(fig, tooltip)
    # ax.plot(rt,map(lambda x:float(x),rx), label='rx')
    # ax.plot(rt,map(lambda x:float(x),ry), label='ry')
    # ax.plot(rt,map(lambda x:float(x),rz), label='rz')
    # ax.legend(loc=1,borderaxespad=0.) # label x y z
    ax.set_title(u+"("+h+") at "+str(len(uni_loc))+" unique locations ("+",".join(uni_loc)+")")
    ax.set_xlabel("Motion from "+start+" to "+end+"; user info: "+"(room"+r+','+m+")") # sensor label
    plt.grid(True)
plt.tight_layout()
# plt.show()
# mpld3.display()

if(args.html):
	outfile=infile.split('/')[-1].replace('.json','.html')
	with open(outfile_path+outfile,'w') as f:
	    f.write(mpld3.fig_to_html(fig))
else:
	outfile=infile.split('/')[-1].replace('.json','.png')
	plt.savefig(outfile_path+outfile)
