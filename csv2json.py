import argparse
from datetime import datetime
import json

# 
# Read session: read all necessary files including rpi, wristband and motion information
# 
split_list=lambda x,dim:x.replace('\n','').replace('"','').replace(':','').split(dim)
parser = argparse.ArgumentParser()
parser.add_argument("infile", help="input the motion file")
parser.add_argument("outpath", help="specify output directory")
parser.add_argument('-k','--key',default='nil',help="group motion data by [nil/mac_address/user_name/room_number/floor_number], default is nil")
parser.add_argument('-d','--delimiter',default='\t',help="split csv file with delimiter [\t/,], default is \t")
parser.add_argument('-m','--mask',action='store_false',help="mask service user name with character x, default is true")
args = parser.parse_args()
infile=args.infile
outfile_prefix=infile.split('/')[-1].replace('.csv','')
outfile_path=args.outpath

with open('/home/ics/Downloads/piegon/info/wristband_info.txt','r') as f:
    wristband = {split_list(x,'\t')[0]:{
        'user_id':split_list(x,'\t')[1],
        'wristband_name':split_list(x,'\t')[2],
        'user_name':split_list(x,'\t')[3].decode('utf-8') if args.mask else split_list(x,'\t')[3].decode('utf-8'),
        'hand':split_list(x,'\t')[4].decode('utf-8'),
        'room_number':split_list(x,'\t')[5],
        'floor_number':split_list(x,'\t')[6]
    } for x in f.readlines()[1:]}
with open('/home/ics/Downloads/piegon/info/rpi_info.txt','r') as f:
    rpi = {split_list(x,'\t')[0]:{
        'floor_number':split_list(x,'\t')[1],
        'room_number':split_list(x,'\t')[2],
        'location_name':split_list(x,'\t')[3]
    } for x in f.readlines()[1:]}
with open(infile,'r') as f:
    data = sorted(map(lambda x:split_list(x,args.delimiter),f.readlines()[1:]),key=lambda x:float(x[0])) #[1:]

# 
# Data crunching session: transform array data to dictionary
# 

def getDuration(motion,rpi_lookup):
    get_ele=lambda arr,key:map(lambda x:x[key],arr)
    get_tuple=lambda record: [rpi_lookup[record[3]]['location_name'],record[0]]
    remove_dup_loc=lambda x,y:x+[y] if x[-1][0]!=y[0] else x
    start_location=reduce(remove_dup_loc,map(get_tuple,a_motion),[['','']])[1:]
    end_location=reduce(remove_dup_loc,map(get_tuple,a_motion[::-1]),[['','']])[1:][::-1]
    return map(lambda (x,y):{'location':x[0],
                             'start_time':x[1],
                             'end_time':y[1]},zip(start_location,end_location))

# Initialize dictionary with mac_address as key
motion_dict={d[4]:[] for d in data[1:]}

for key in motion_dict.keys():
    wristband_motion=filter(lambda x:x[4]==key, data)
    a_motion=filter(lambda x:x[4]==key and x[1]=='A' , wristband_motion)
    g_motion=filter(lambda x:x[4]==key and x[1]=='G' , wristband_motion)
    motion_dict[key]={
        'mac_address':key,
        'wristband_name':wristband[key]['wristband_name'],
        'user_name':wristband[key]['user_name'],
        'hand':wristband[key]['hand'],
        'room_number':wristband[key]['room_number'],
        'floor_number':wristband[key]['floor_number'],
        'location_paths':getDuration(wristband_motion,rpi),
        't':map(lambda x:x[0],a_motion),
        'x':map(lambda x:x[5],a_motion),
        'y':map(lambda x:x[6],a_motion),
        'z':map(lambda x:x[7],a_motion),
        'idx':map(lambda x:x[2],a_motion),
        'rt':map(lambda x:x[0],g_motion),
        'rx':map(lambda x:x[5],g_motion),
        'ry':map(lambda x:x[6],g_motion),
        'rz':map(lambda x:x[7],g_motion),
        'ridx':map(lambda x:x[2],g_motion)
    }
# 
# Export Session: split data into user defined conditions, e.g. group by user name, room_number and floor number etc.
#
opt=filter(lambda x:x[0]==args.key,[
    ('nil',lambda u,k:''),
    ('user_name',lambda u,k:'_'+u[k]['room_number']+'_'+u[k]['user_name']),
    ('mac_address',lambda u,k:'_'+u[k]['room_number']+'_'+u[k]['mac_address']),
    ('room_number',lambda u,k:'_'+u[k]['room_number']),
    ('floor_number',lambda u,k:'_'+u[k]['floor_number'])])[0]
filter_condition=lambda x,y:opt[0]=='nil' or x[opt[0]]==y[opt[0]]
grouped_motions = [{k: v for k, v in motion_dict.items() if filter_condition(v,motion_dict[key])} for key in motion_dict.keys()]
for g in grouped_motions:
    user_key=g.keys()[0]
    outfile_suffix=opt[1](g,user_key)
    outfile=outfile_prefix+outfile_suffix+'.json'
    g['file_name']=outfile
    with open(outfile_path+outfile,'w') as f:
        json.dump(g,f,indent=4)
