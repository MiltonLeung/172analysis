from sqlalchemy import *
import datetime
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from sklearn.decomposition import PCA
from scipy.cluster.vq import kmeans,vq

class motionUtil():

    sampling_rate=52
    windows=5
    overlap=0
    sample={}
    segment_info={}
    from_datetime=lambda self,x:datetime.datetime.fromtimestamp(float(x))
    to_unixtime=lambda self,dt:int(dt.strftime("%s"))
    wristband_list=[]
    color=['brown','red','darkorange','gold','green','springgreen','cyan','skyblue','blue','darkviolet','violet','purple']
    feature_list=['min', 'max', 'mean', 'variance', 'skewness', 'kurtosis', 'q1', 'q2', 'q3','abs_mean','cf','min', 'max', 'mean', 'variance', 'skewness', 'kurtosis', 'q1', 'q2', 'q3','abs_mean','cf','min', 'max', 'mean', 'variance', 'skewness', 'kurtosis', 'q1', 'q2', 'q3','abs_mean','cf', 'rms','sma','corr_xy','corr_xz','corr_yz']
    np.random.seed(42)
    signal_delay_threshold=10*1/float(sampling_rate)
    def __init__(self,wristband_list,windows=5,overlap=0):
        self.windows=windows
        self.overlap=overlap
        self.wristband_list=wristband_list
        self.sample={}
        self.segment_info={}
        signal_delay_threshold=10*1/float(self.sampling_rate)

    def fetch_data_from_mysql(self,start,end,table="motion"):
        start_datetime=start.split(' ')[0]
        db=create_engine("mysql://ics:projectARD172@10.6.49.84/"+table+"?charset=utf8")
        connect = db.connect()
        wristband_id = ','.join(map(lambda x:'\''+x+'\'',self.wristband_list)) 
        sql="SELECT unix_timestamp,sensor_type,msg_idx,rpi_id,wristband_id,x,y,z from Motion where unix_timestamp between UNIX_TIMESTAMP(STR_TO_DATE('"+start+"', '%%Y-%%m-%%d %%H:%%i')) and UNIX_TIMESTAMP(STR_TO_DATE('"+end+"', '%%Y-%%m-%%d %%H:%%i')) and wristband_id in ("+wristband_id+") order by unix_timestamp;"
        result=db.engine.execute(sql).fetchall()
        for record in result:
            key=line.split('\t')[4]
            if(self.sample.has_key(key)):
                self.sample[key].append(record)
            else:
                self.sample[key]=[record]
        user_timeframe=([x for s,e in self.getTimeTable(start_datetime.split(' ')[0],db) for x in (self.sliding_windows(s,e,self.windows,self.overlap)) ])
        for key in self.sample.keys():
            self.segment_info=filter(lambda x: x[0]>=self.sample[key][0][0] and x[0]<=self.sample[key][-1][0],self.get_features(self.sample,key,user_timeframe))
    def fetch_data_from_csv(self,file):
        parse_line=lambda x:[float(x[0]),x[1],x[2],x[3],x[4],float(x[5]),float(x[6]),float(x[7])]
        for line in open(file):
            key=line.split('\t')[4]
            if(len(self.wristband_list)==0 or len(filter(lambda x:x==key,self.wristband_list))!=0):
                record=line.split('\t')
                record[0]=float(record[0])
                record[5]=float(record[5])
                record[6]=float(record[6])
                record[7]=float(record[7])
                if(self.sample.has_key(key)):
                    self.sample[key].append(record)
                else:
                    self.sample[key]=[record]
        for key in self.sample.keys():
            self.sample[key].sort(key=lambda x: x[0])
            user_timeframe=self.sliding_windows(self.sample[key][0][0],self.sample[key][-1][0],self.windows,self.overlap)
            self.segment_info[key]=self.get_features(self.sample,key,user_timeframe)
    def motion_plot(self,motion,plt):
        x=map(lambda x:x[5],motion)
        y=map(lambda x:x[6],motion)
        z=map(lambda x:x[7],motion)
        t=map(lambda x:(self.from_datetime(x[0])),motion)
        plt.plot(t,x,t,y,t,z)
    # Acceleration Vector Changes - The fiercer the motion, the greater the change in the signal.
    def AVC(self,motion):
        gravity=9.8
        length=lambda x:np.sqrt(x[5]**2+x[6]**2+x[7]**2)
        sm=[0 if i==0 else abs(length(motion[i])-length(motion[i-1])) for i in range(len(motion))]
        avc=sum(sm)/float(len(motion)+0.00001)
        return avc
    # Partition time frame into defined window size and overlap size in seconds
    def sliding_windows(self,start_day,end_day, windows,overlap):
        total_seconds = float(end_day-start_day)
        step_count=map(lambda x:x*(windows-overlap),range(0, int((total_seconds-windows)/(windows-overlap))+1))
        return [(start_day + x,start_day + x + windows) for x in step_count]
    def binSearch(self,a, value):
        lo = 0
        hi = len(a) - 1
        lastValue = 0
        mid = (lo + hi) / 2
        while (lo <= hi) :
            mid = (lo + hi) / 2
            lastValue = a[mid][0]
            if (value < lastValue):
                hi = mid - 1
            elif (value > lastValue):
                lo = mid + 1
            else:
                return mid
        return mid
    def filter_motion(self,motion,s,e):
        start=self.binSearch(motion,s)
        end=self.binSearch(motion,e)
        return motion[start:end]
    def get_features(self,motion,wristband,sliding_windows):
        result=[]
        for start,end in sliding_windows:
            segment=self.filter_motion(motion[wristband],start,end)
            result.append((start,end,self.AVC(segment),len(segment),wristband))
        return result
    def print_truple(self,x):
        # print str(from_datetime(x[0]))+' '+str(from_datetime(x[1]))+' '+str(x[2])
        return x
    def getTimeTable(self,date,db):
        timeframe=30
        sql="SELECT * from healthcare.Timetable where time ='"+date+"'"
        timetable=db.engine.execute(sql).fetchall()
        table_converter=lambda x:(self.to_unixtime(timetable[0][1]+datetime.timedelta(minutes=x)),self.to_unixtime(timetable[0][1]+datetime.timedelta(minutes=x+timeframe)))
        return map(table_converter,filter(lambda x:x>-1,[ i*timeframe if timetable[0][2][i]=='1' else -1 for i in range(len(timetable[0][2]))]))
    def avcPlot(self,wristband):
        fig = plt.figure( figsize=(17, 1.5*4), dpi=80)
        plt.subplot(211)
        self.motion_plot(self.sample[wristband],plt)
        plt.subplot(212)
        plt.plot(map(lambda x:self.from_datetime(x[0]),self.segment_info[wristband]),map(lambda x:x[2],self.segment_info[wristband]))
        plt.tight_layout()
        plt.show()
    def SMA(self,motion):
        vector_sum=lambda x:abs(x[5])+abs(x[6])+abs(x[7])
        sm=reduce(lambda x,y:x+vector_sum(y),motion,0)
        return sm/float(len(motion))
    def outlier(self,segment):
        data=(map(lambda x:(x[3]),filter(lambda x:x[3]>0,segment)))
        q3=np.percentile(data,75)
        q1=np.percentile(data,25)
        k=1.5
        turkey_range=(q1-k*(q3-q1),q3+k*(q3-q1))
        return turkey_range
    def get_movement_segment(self,wristband,threshold):
        movement_segment=filter(lambda x:x[2]>=threshold,self.segment_info[wristband])
        print 'Movement before outlier removal: ',len(movement_segment)
        if(len(movement_segment)!=0):
            lq,uq=self.outlier(movement_segment)
            movement_segment=filter(lambda x:x[3]>=lq and x[3]<=uq,movement_segment)
            print 'Turkey range: ',lq,uq
            print 'Movement after outlier removal: ',len(movement_segment)
            # movement_segment=filter(lambda x:x[3]>=lq and x[3]<=uq,movement_segment)

            # max([ m[i][0]-m[i-1][0] for i in range(1,len(movement_segment))])
        return movement_segment
    def get_feature_vector(self,motion_list):
        x=map(lambda x:x[5],motion_list)
        y=map(lambda x:x[6],motion_list)
        z=map(lambda x:x[7],motion_list)
        abs_sum=lambda x:abs(x[5])+abs(x[6])+abs(x[7])
        magnitude=map(abs_sum,motion_list)
        stat=stats.describe(magnitude)
        stat_x=stats.describe(x)
        stat_y=stats.describe(y)
        stat_z=stats.describe(z)
        q1_x=np.percentile(x,25)
        q2_x=np.percentile(x,50)
        q3_x=np.percentile(x,75)
        q1_y=np.percentile(y,25)
        q2_y=np.percentile(y,50)
        q3_y=np.percentile(y,75)
        q1_z=np.percentile(z,25)
        q2_z=np.percentile(z,50)
        q3_z=np.percentile(z,75)
        abs_mean_x=sum(map(lambda m:abs(m),x))/len(x)
        abs_mean_y=sum(map(lambda m:abs(m),y))/len(y)
        abs_mean_z=sum(map(lambda m:abs(m),z))/len(z)
        cf_x=stat_x.minmax[1]/abs_mean_x
        cf_y=stat_y.minmax[1]/abs_mean_y
        cf_z=stat_z.minmax[1]/abs_mean_z
        corr=np.corrcoef([x,y,z])
        corr_xy=corr[1,0]
        corr_xz=corr[2,0]
        corr_yz=corr[2,1]
        rms=np.sqrt(np.mean(np.square(magnitude)))
        sma=sum(map(abs_sum,motion_list))/float(len(motion_list))
        return [stat_x.minmax[0],stat_x.minmax[1],stat_x.mean,stat_x.skewness,stat_x.variance,stat_x.kurtosis,q1_x,q2_x,q3_x,abs_mean_x,cf_x,stat_y.minmax[0],stat_y.minmax[1],stat_y.mean,stat_y.skewness,stat_y.variance,stat_y.kurtosis,q1_y,q2_y,q3_y,abs_mean_y,cf_y,stat_z.minmax[0],stat_z.minmax[1],stat_z.mean,stat_z.skewness,stat_z.variance,stat_z.kurtosis,q1_z,q2_z,q3_z,abs_mean_z,cf_z,rms,sma,corr_xy,corr_xz,corr_yz]
    def get_movement_feature(self,wristband,segment_list):
        return [{'info':segment,'features':self.get_feature_vector(self.filter_motion(self.sample[wristband],segment[0],segment[1]))} for segment in segment_list]
    def normalize_vector(self,feature_vector):
        boxcox = lambda x:stats.boxcox(map(lambda y:abs(y+0.000001),x))[0].tolist() # feature_vector
        normalized_vector=np.transpose(map(boxcox,np.transpose(map(lambda x:x['features'],feature_vector)).tolist()))
        return map(lambda x: {'info':x[0],'features':x[1]},zip(map(lambda x:x['info'],feature_vector),normalized_vector))
    def PCA_kmeans(self,feature_vector,cluster,color='blue'):
        pca = PCA(n_components=6)
        fig = plt.figure( figsize=(8.5, 10), dpi=80)
        X = pca.fit_transform(map(lambda x:x['features'],feature_vector))
        print 'Explained variance',pca.explained_variance_ratio_
        plt.subplot(2,1,1)
        plt.scatter(map(lambda x:x[0],X),map(lambda x:x[1],X),color=color)
        centroids,_ = kmeans(X,cluster)
        idx,_ = vq(X,centroids)
        plt.subplot(2,1,2)
        plt.scatter(map(lambda x:x[0],X),map(lambda x:x[1],X),color=map(lambda x:self.color[x],idx))
        plt.plot(centroids[:,0],centroids[:,1],'sg',markersize=8)
        return idx